import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Navbar from './Components/Navbar';
import Home from './Page/Home';
import { Login } from './Page/Login-page';
import RegisterFormulaire from './Page/RegisterFormulaire';





function App() {
  return (
    
    
      
    <div>
       
      
      <BrowserRouter>
     
      
    <Navbar/>
    
        <Switch>
          <Route exact path='/'>
            <Home/>
          </Route>
          <Route exact path= '/login'>
           <Login/>
          </Route>
          <Route exact path= '/register'>
           <RegisterFormulaire/>
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
