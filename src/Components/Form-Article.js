import { useState } from "react";





export function FormArticle({onFormSubmit}) {


const [form,setForm]=useState({
    description:"",
    titre:"",
    Images:"",
    auteur:"",
    date:"",
})
    function handleChange(event) {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        onFormSubmit(form);
    }

    return (
        <form onSubmit={handleSubmit}>
            <label> Titre :</label>
            <input className="form-control" required type="text" name="titre" onChange={handleChange} value={form.titre} />
            <label>Auteur :</label>
            <input className="form-control" required type="text" name="auteur" onChange={handleChange} value={form.auteur} />
            <label>Date :</label>
            <input className="form-control" type="date" name="date" onChange={handleChange} value={form.date} />
            <label>Description :</label>
            <input className="form-control" required type="text" name="description" onChange={handleChange} value={form.description} />
            <label>Images :</label>
            <input className="form-control" required type="text" name="Images" onChange={handleChange} value={form.Images} />
            <button className="btn btn-primary">Poster un Article</button>
        </form>
    );
}
