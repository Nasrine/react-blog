import React from 'react';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { makeStyles } from '@material-ui/core/styles';
import { useState } from 'react';
import { ArticleBlog } from '../Service/ArticleBlog';
import { useEffect } from 'react';
import { Box } from '@material-ui/core';





const titreimage = makeStyles((theme) => ({
  root: {
    maxWidth: 340,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function Article() {
  const classes = titreimage();
  const [expanded, setExpanded] = React.useState(false); 
  const [Articles, setArticles ] = useState ([])
 
  async function fetchArticles(){
      const reponse = await ArticleBlog.fetchAllArticle();
      setArticles (reponse)
  }
useEffect(()=>{fetchArticles()},[]);
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <div>
          <Box display="flex" flexDirection="row" p={3} m={3} justifyContent="space-between">

        {Articles.map(item =>   <Card key={item.id} className={classes.root}> <CardHeader 
        avatar={
          <Avatar aria-label="recipe" className={classes.avatar} key="item.id">
            R
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={item.titre}
        subheader={item.date}
      />
     {item.Images && <CardMedia
     component="img"
        className={classes.image}
        image={item.Images}
        title="Clavier"
      />
     }
      <CardContent>
        <Typography variant="body2"  component="p">
          {item.description}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
   
    
    </Card>
        )}
    </Box>
    </div>       
  
)}