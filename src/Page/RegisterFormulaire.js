import { useDispatch, useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { register } from '../stores/auth-slice';
import { useState } from 'react';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

    

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function RegisterFormulaire({ onFormSubmit }) {
  const classes = useStyles();
   const [form, setForm] = useState({});

  function handlechange (event) {
    console.log(form);
    setForm({
      ...form,
      [event.target.name]: event.target.value
    });
  }

  // function handleSubmit(event) {
  //   event.preventDefault();
  //   onFormSubmit(form);
  // }
  // const dispatch = useDispatch();
  //   const [feedback, setFeedback] = useState('');
  //   const registerRequest = async (data) => {
  //       try {

  //           dispatch(register(data));

  //           setFeedback('Hello' + data.email)
  //       } catch (error) {
  //           setFeedback('Credentials error')

  //       }
  //   }
const dispatch= useDispatch()
const feedback= useSelector(state=> state.auth.registerFeedback)
const onFinish=(event)=>{
  dispatch(register(form))
  event.preventDefault();
  
}

  return (
    <Container component="main" maxWidth="xs">
      {feedback && <p>{feedback}</p>}
      <CssBaseline />
      <div className={classes.paper}>
        
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} onSubmit={onFinish} noValidate>
          <Grid container spacing={2}>
            
            
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                onChange={handlechange}
                required
                fullWidth
                id="email"
                label="Email Address"
                name="mail"
                autoComplete="email"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                onChange={handlechange}
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
              />
            </Grid>
            
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            
          >
            Sign Up
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link href="#" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}