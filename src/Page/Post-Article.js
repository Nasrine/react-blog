import { useDispatch } from "react-redux";
import { FormArticle } from "../Components/Form-Article";
import { addpost } from "../stores/article-slice";


export function PageArticle() {
   const dispatch = useDispatch()

    
    const add=(values)=> {
      dispatch(addpost(values))
    }
  
  return (
    <div>
      <FormArticle onFormSubmit= {add} />
      
    </div>
  )
}
