import { useState } from "react";
import { AuthService } from "../Service/auth-service";
import { useDispatch } from "react-redux";
import { login } from "../stores/auth-slice";
import { useHistory } from "react-router-dom";
import SignIn from "../Components/LoginForm";




export function Login() {
    let history = useHistory();
    function handleClick() {
        // le nom de ma page
        history.push("/");
      }

 


    const dispatch = useDispatch();
    const [feedback, setFeedback] = useState('');
    const loginRequest =  async (credentials) => {
        try {
            const user = await AuthService.login(credentials);
             //Ici, on utilise le dispatch pour déclencher l'action login en lui fournissant
            //comme payload le user qui vient de s'authentifié
            dispatch(login(user));
            setFeedback('Hello '+user.email)
            handleClick()


        } catch (error) {
            setFeedback('Credentials error');
        }

    };

    return (

        <div>
            {feedback}
            <SignIn onFormSubmit={loginRequest}  />
        </div>
    )
}