import { createSlice } from "@reduxjs/toolkit";
import { AuthService } from "../Service/auth-service";




const authSlice = createSlice({
    name: 'auth',
    initialState: {
        user: false,
        loginFeedback: '',
        registerFeedback: ''
    },
    reducers: {
        login(state, {payload}) {
            state.user = payload;
        },
        logout(state) {
            state.user = null;
            localStorage.removeItem('token');
        },
        updateLoginFeedback(state, {payload}) {
            state.loginFeedback = payload
        },
        updateRegisterFeedback(state, {payload}) {
            state.registerFeedback = payload
        }
    }
});
/**
 * On exporte les différentes actions afin de pouvoir s'en servir
 * facilement depuis les components
 */
export const {login,logout, updateLoginFeedback, updateRegisterFeedback} = authSlice.actions;

//On export le reducer pour la charger dans le store
export default authSlice.reducer;


export const loginWithToken = () => async (dispatch) => {
    const token = localStorage.getItem('token');

    if(token) {
        try {

            const user = await AuthService.fetchAccount();

            dispatch(login(user));
        } catch(error) {
            dispatch(logout());
        }
    } else {

        dispatch(logout());
    }
    
}

export const register = (body) => async (dispatch) => {
    try {
        const user = await AuthService.register(body);

        dispatch(login(user));

        // dispatch(loginWithCredentials(body));

    } catch (error) {
        console.log(error)
        
    }
}

export const loginWithCredentials = (credentials) => async (dispatch) => {
    try {
        const user = await AuthService.login(credentials);
        dispatch(updateLoginFeedback('Login successful'));
        dispatch(login(user));

    } catch (error) {
        dispatch(updateLoginFeedback('Email and/or password incorrect'));
    }
}
