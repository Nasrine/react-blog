import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    list: []
};
/**
 * La UserSlice, malgré son nom, n'est pas du tout liée à l'authentification.
 * On en a à priori pas besoin dans les projets, à part si on veut créer
 * une page d'administration où les admin pourront consulter la liste
 * des users enregistrés, sinon, pas besoin
 */
const userSlice = createSlice({
    name: 'users',
    initialState,
    reducers: {
        setList(state, {payload}) {
            state.list = payload;

        },
        addUser(state, { payload }) {
            state.list.push(payload);
    }
}
});

export const {setList,addUser} = userSlice.actions;

export default userSlice.reducer;


export const fetchUsers = () => async (dispatch) => {
    try {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/user');
        dispatch(setList(response.data));
    } catch (error) {
        console.log(error);
    }
}
