import { createSlice } from "@reduxjs/toolkit";
import { AuthService } from "../Service/auth-service";


const ArticleSlice = createSlice({

    name: "article",
    initialState : { liste:  []},
    reducers: {
        // rajouter des virgule apres chaque reducer
        setArticle (state,{payload}){
            state.liste=payload
        },
        //  une methode pour poster un article sur le blogue (qui s'apel add)
        //je l'apel comme je veu c'est  juste le nom de la function ou de la variable 
        addArticle (state,{payload}){
            state.liste.push(payload)
        }
        
    }
});
export const {setArticle,addArticle}= ArticleSlice.actions;
export default ArticleSlice.reducer;
export const fetchpost=()=> async (dispatch) => {
    const response = await AuthService.fetchPost()
    dispatch (setArticle(response))

}
 export const addpost = (post) => async (dispatch) => {
     try {
         const resp = await AuthService.addpost(post)
         dispatch (addArticle(resp.data))
     }
     catch (error){console.log(error)}
 }
 