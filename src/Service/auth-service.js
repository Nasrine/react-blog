import axios from "axios";



export class AuthService {


    static async register(user) {
        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/blog/user', user);

        return response.data;
    }


    static async login(credentials) {
        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/blog/user/login', credentials);
        /**
         * Seul truc particulier de la méthode login par rapport aux autres requête est
         * que l'on va récupérer le token si le login fonctionne et le stocker en localStorage
         * (c'est à dire dans le navigateur, afin de pouvoir s'en servir, même si on recharge
         * la page ou qu'on ferme et relance le navigateur/l'onglet)
         */
        localStorage.setItem('token', response.data.token);
        
        return response.data.user;
    }

    static async fetchAccount() {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/blog/user/account');
        
        return response.data;
        
    }
static async  fetchPost () {
    const response = await axios.get (process.env.REACT_APP_SERVER_URL+'/api/blog/article');
    return response.data;
}
static async addpost (article) {
    const response = await axios.post (process.env.REACT_APP_SERVER_URL+'/api/blog/article',article);
    return response.data;
}
}
