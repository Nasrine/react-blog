import axios from "axios";
import { logout } from "../stores/auth-slice";
import {store} from '../stores/store'

axios.interceptors.request.use((config) => {
    const token =localStorage.getItem('token');
    if(token) {

        config.headers.authorization = 'bearer '+token;
    }
    return config;
});

axios.interceptors.response.use((response) => {
    return response;
}, (error) => {
        
    if(error.response.status === 401) {
        store.dispatch(logout());
    }
})
