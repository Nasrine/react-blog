import axios from 'axios'

export class ArticleBlog{
    static async fetchAllArticle() {
        const reponse = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/blog/article/all');
        return reponse.data;
    }
    static async add(article) {
        return axios.post(process.env.REACT_APP_SERVER_URL+'/api/blog/article',article);
    }
}